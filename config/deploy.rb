lock '3.8.0'

set :application, 'file-manager'
set :repo_url, 'git@bitbucket.org:inacio_farias/file-manager.git'
set :deploy_to, '/home/deploy/file-manager'

append :linked_files, 'config/database.yml', 'config/secrets.yml'
append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system', 'uploaded_files'
