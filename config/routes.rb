Rails.application.routes.draw do
  devise_for :users

  resource :home, only: :index

  resources :folders, except: [:new, :edit] do
    member do
      post 'create', as: :new_inner
      get  :manage
    end

    resources :access_permits, only: [:create, :update, :destroy]
  end

  resources :files, only: [:destroy] do
    post :upload, on: :collection

    member do
      get  :download
      post :toggle_lock
    end
  end

  resource :current_user, only: [:update], controller: :current_user do
    collection do
      get   :profile
      put   :update_password
      patch :update_password
    end
  end

  resource :control_panel, only: :show, controller: :control_panel do
    resources :users, except: :show do
      member do
        put   :update_password
        patch :update_password
      end
    end

    resources :contacts, only: [:index, :create, :destroy] do
      member do
        post :mark_as_processed
        post :mark_as_unprocessed
      end
    end
  end

  root 'home#index'
end
