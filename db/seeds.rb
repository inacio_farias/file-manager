User.create(
  name: 'Admin',
  email: 'admin@staysafe.com.br',
  password: 'stsf-admin@fm',
  password_confirmation: 'stsf-admin@fm',
  admin: true
)

User.create(
  name: 'Guest1',
  email: 'guest1@staysafe.com.br',
  password: 'guest1-psw',
  password_confirmation: 'guest1-psw'
)

User.create(
  name: 'Guest2',
  email: 'guest2@staysafe.com.br',
  password: 'guest2-psw',
  password_confirmation: 'guest2-psw'
)

User.create(
  name: 'Guest3',
  email: 'guest3@staysafe.com.br',
  password: 'guest3-psw',
  password_confirmation: 'guest3-psw'
)
