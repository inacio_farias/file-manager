class AddOriginalNameToAsset < ActiveRecord::Migration[5.0]
  def change
    add_column :assets, :original_name, :string
  end
end
