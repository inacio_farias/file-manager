class CreateLogAccessPermits < ActiveRecord::Migration[5.0]
  def change
    create_table :log_access_permits do |t|
      t.string :type
      t.text :recorded_changes
      t.integer :access_permit_id, null: false, index: true
      t.integer :user_id,          null: false, index: true

      t.timestamps
    end
  end
end
