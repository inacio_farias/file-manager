class AddOwnerIdToFolders < ActiveRecord::Migration[5.0]
  def change
    add_column :folders, :owner_id, :integer, index: true
    add_foreign_key :folders, :users, column: :owner_id
  end
end

=begin

SCRIPT TO FILL FOLDER'S OWNER_ID FIELD

Folder.where(:parent_id => nil).each do |f|
  f.self_and_descendants.update_all(:owner_id => f.creator_id)
end

=end
