class CreateLogAssets < ActiveRecord::Migration[5.0]
  def change
    create_table :log_assets do |t|
      t.string :type
      t.text :recorded_changes
      t.integer :asset_id, null: false, index: true
      t.integer :user_id,  null: false, index: true

      t.timestamps
    end
  end
end
