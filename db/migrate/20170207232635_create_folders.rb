class CreateFolders < ActiveRecord::Migration[5.0]
  def change
    create_table :folders do |t|
      t.string  :name
      t.integer :creator_id, index: true
      t.integer :parent_id,  index: true

      t.timestamps
    end

    add_foreign_key :folders, :users, column: :creator_id
    add_foreign_key :folders, :folders, column: :parent_id
  end
end
