class CreateContact < ActiveRecord::Migration[5.0]
  def change
    create_table :contacts do |t|
      t.string   :name,      null: false
      t.string   :email,     null: false
      t.integer  :origin,    null: false
      t.datetime :processed_at

      t.timestamps
    end
  end
end
