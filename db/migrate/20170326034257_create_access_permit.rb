class CreateAccessPermit < ActiveRecord::Migration[5.0]
  def change
    create_table :access_permits do |t|
      t.belongs_to :resource, index: true, polymorphic: true
      t.belongs_to :user, index: true
      t.integer :access_level

      t.timestamps
    end
  end
end
