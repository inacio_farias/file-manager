class CreateAssetLock < ActiveRecord::Migration[5.0]
  def change
    create_table :asset_locks do |t|
      t.integer :asset_id, index: true, null: false
      t.integer :user_id,  index: true, null: false
    end

    add_foreign_key :asset_locks, :assets, column: :asset_id
    add_foreign_key :asset_locks, :users,  column: :user_id
  end
end

