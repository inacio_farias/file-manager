class AddUserLevelToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :user_level, :integer, default: 0
  end
end

=begin

SCRIPT TO FILL USER'S USER_LEVEL FIELD

scope = User.with_deleted
scope.where(admin: true).update_all user_level: User::USER_LEVELS.index(:super)
scope.where(admin: false).update_all user_level: User::USER_LEVELS.index(:regular)

=end
