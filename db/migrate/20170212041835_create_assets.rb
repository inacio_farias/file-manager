class CreateAssets < ActiveRecord::Migration[5.0]
  def change
    create_table :assets do |t|
      t.integer :user_id, index: true, null: false
      t.integer :folder_id, index: true

      t.timestamps
    end

    add_foreign_key :assets, :users, column: :user_id
    add_foreign_key :assets, :folders, column: :folder_id
  end
end
