# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170516011433) do

  create_table "access_permits", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "resource_type"
    t.integer  "resource_id"
    t.integer  "user_id"
    t.integer  "access_level"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["resource_type", "resource_id"], name: "index_access_permits_on_resource_type_and_resource_id", using: :btree
    t.index ["user_id"], name: "index_access_permits_on_user_id", using: :btree
  end

  create_table "asset_locks", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "asset_id", null: false
    t.integer "user_id",  null: false
    t.index ["asset_id"], name: "index_asset_locks_on_asset_id", using: :btree
    t.index ["user_id"], name: "index_asset_locks_on_user_id", using: :btree
  end

  create_table "assets", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "user_id",                    null: false
    t.integer  "folder_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "uploaded_file_file_name"
    t.string   "uploaded_file_content_type"
    t.integer  "uploaded_file_file_size"
    t.datetime "uploaded_file_updated_at"
    t.string   "original_name"
    t.datetime "deleted_at"
    t.index ["deleted_at"], name: "index_assets_on_deleted_at", using: :btree
    t.index ["folder_id"], name: "index_assets_on_folder_id", using: :btree
    t.index ["user_id"], name: "index_assets_on_user_id", using: :btree
  end

  create_table "contacts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name",         null: false
    t.string   "email",        null: false
    t.integer  "origin",       null: false
    t.datetime "processed_at"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "folder_hierarchies", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "ancestor_id",   null: false
    t.integer "descendant_id", null: false
    t.integer "generations",   null: false
    t.index ["ancestor_id", "descendant_id", "generations"], name: "folder_anc_desc_idx", unique: true, using: :btree
    t.index ["descendant_id"], name: "folder_desc_idx", using: :btree
  end

  create_table "folders", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "name"
    t.integer  "creator_id"
    t.integer  "parent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
    t.integer  "owner_id"
    t.index ["creator_id"], name: "index_folders_on_creator_id", using: :btree
    t.index ["deleted_at"], name: "index_folders_on_deleted_at", using: :btree
    t.index ["owner_id"], name: "fk_rails_2135f0fb82", using: :btree
    t.index ["parent_id"], name: "index_folders_on_parent_id", using: :btree
  end

  create_table "log_access_permits", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "type"
    t.text     "recorded_changes", limit: 65535
    t.integer  "access_permit_id",               null: false
    t.integer  "user_id",                        null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.index ["access_permit_id"], name: "index_log_access_permits_on_access_permit_id", using: :btree
    t.index ["user_id"], name: "index_log_access_permits_on_user_id", using: :btree
  end

  create_table "log_assets", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "type"
    t.text     "recorded_changes", limit: 65535
    t.integer  "asset_id",                       null: false
    t.integer  "user_id",                        null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.index ["asset_id"], name: "index_log_assets_on_asset_id", using: :btree
    t.index ["user_id"], name: "index_log_assets_on_user_id", using: :btree
  end

  create_table "log_folders", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "type"
    t.text     "recorded_changes", limit: 65535
    t.integer  "folder_id",                      null: false
    t.integer  "user_id",                        null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.index ["folder_id"], name: "index_log_folders_on_folder_id", using: :btree
    t.index ["user_id"], name: "index_log_folders_on_user_id", using: :btree
  end

  create_table "log_users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "type"
    t.text     "recorded_changes", limit: 65535
    t.integer  "target_id",                      null: false
    t.integer  "user_id",                        null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.index ["target_id"], name: "index_log_users_on_target_id", using: :btree
    t.index ["user_id"], name: "index_log_users_on_user_id", using: :btree
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "name",                   default: "",    null: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.datetime "deleted_at"
    t.boolean  "admin",                  default: false, null: false
    t.integer  "user_level",             default: 0
    t.index ["deleted_at"], name: "index_users_on_deleted_at", using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "asset_locks", "assets"
  add_foreign_key "asset_locks", "users"
  add_foreign_key "assets", "folders"
  add_foreign_key "assets", "users"
  add_foreign_key "folders", "folders", column: "parent_id"
  add_foreign_key "folders", "users", column: "creator_id"
  add_foreign_key "folders", "users", column: "owner_id"
end
