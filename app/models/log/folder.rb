module Log
  class Folder < ApplicationRecord
    belongs_to :folder, class_name: '::Folder'
    belongs_to :user

    serialize :recorded_changes, Hash

    before_save :ignore_timestamps!

    private

    def ignore_timestamps!
      recorded_changes.delete('created_at')
      recorded_changes.delete('updated_at')
    end
  end
end
