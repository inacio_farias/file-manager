module Log
  class User < ApplicationRecord
    belongs_to :target, class_name: '::User'
    belongs_to :user

    serialize :recorded_changes, Hash

    before_save :ignore_timestamps!
    before_save :ignore_password!

    private

    def ignore_timestamps!
      recorded_changes.delete('created_at')
      recorded_changes.delete('updated_at')
    end

    def ignore_password!
      recorded_changes.delete('encrypted_password')
    end
  end
end
