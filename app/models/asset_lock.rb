class AssetLock < ApplicationRecord
  belongs_to :user
  belongs_to :asset

  validates_uniqueness_of :asset_id

  def user_name
    user.name
  end

  def asset_name
    asset.original_name
  end
  alias file_name asset_name
end
