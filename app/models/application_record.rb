class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def fetch(*arguments)
    method, *args, default = arguments
    send(method, *args) || default
  end
end
