class Asset < ApplicationRecord
  acts_as_paranoid

  belongs_to :user
  belongs_to :folder, optional: true, touch: true

  has_many :logs, class_name: '::Log::Asset'

  has_one :asset_lock

  PAPERCLIP_OPTS = {
    url:  '/files/:id/download',
    path: 'uploaded_files/:id/:basename.:extension'
  }.freeze
  has_attached_file :uploaded_file, PAPERCLIP_OPTS

  validates_attachment_size :uploaded_file, less_than: 500.megabytes
  validates_attachment_presence :uploaded_file
  validates_attachment_content_type :uploaded_file, content_type: /\A.+/
  do_not_validate_attachment_file_type :uploaded_file

  validates_uniqueness_of :original_name, scope: :folder_id,
                                          case_sensitive: false

  after_validation :clean_paperclip_errors

  before_save :protect_locked_asset
  before_destroy :protect_locked_asset

  alias_attribute :file_type, :uploaded_file_content_type
  alias_attribute :file_size, :uploaded_file_file_size

  def locked?
    asset_lock.present?
  end

  private

  def clean_paperclip_errors
    errors.delete(:uploaded_file) unless errors.keys.length == 1
  end

  def protect_locked_asset
    return unless locked?

    msg = 'Não é possível realizar esta ação pois o arquivo foi ' \
          "trancado por #{asset_lock.user_name}"
    errors[:base] << msg
    throw :abort
  end
end
