class User < ApplicationRecord
  acts_as_paranoid

  devise :database_authenticatable, :recoverable, :rememberable,
         :trackable, :validatable, :timeoutable

  has_many :folders, foreign_key: :creator_id
  has_many :assets
  has_many :asset_locks
  has_many :access_permits
  has_many :shared_folders, through: :access_permits,
                            source: :resource,
                            source_type: 'Folder'

  validates_presence_of :name
  validates_presence_of :password_confirmation, on: :create,
                                                if: -> { :password }

  USER_LEVELS = %i[read_only regular super].freeze
  enum user_level: USER_LEVELS

  def admin?
    super || super?
  end
end
