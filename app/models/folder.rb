class Folder < ApplicationRecord
  acts_as_paranoid

  belongs_to :creator, class_name: 'User'
  belongs_to :owner,   class_name: 'User'

  has_many :assets, dependent: :destroy
  has_many :access_permits, as: :resource, dependent: :destroy
  has_many :users, through: :access_permits

  COMMON_OPTS = { through: :access_permits, source: :user }.freeze
  has_many :readers,  -> { merge(AccessPermit.reader)  }, COMMON_OPTS
  has_many :writers,  -> { merge(AccessPermit.writer)  }, COMMON_OPTS
  has_many :managers, -> { merge(AccessPermit.manager) }, COMMON_OPTS

  acts_as_tree dependent: :destroy

  validates_presence_of   :name
  validates_uniqueness_of :name, scope: :parent_id, case_sensitive: false

  before_validation :set_owner_id

  after_update :sync_ancestors_updated_at
  after_touch  :sync_ancestors_updated_at

  before_destroy :ensure_no_locked_file_is_destroyed, prepend: true

  def root?
    false
  end

  def assets_size
    assets.sum(&:file_size)
  end

  def has_locked_file?
    assets.any?(&:locked?)
  end

  def created_by?(user)
    creator == user
  end

  def readable_by?(user)
    owner?(user) || reader?(user) || writer?(user) || manager?(user)
  end
  alias accessible_by? readable_by?

  def writable_by?(user)
    owner?(user) || writer?(user) || manager?(user)
  end

  def manageable_by?(user)
    owner?(user) || manager?(user)
  end

  def owner?(user)
    owner_id == user.id
  end

  private

  def reader?(user)
    user.in?(readers) || ancestors.any? { |a| a.readable_by?(user) }
  end

  def writer?(user)
    user.in?(writers) || ancestors.any? { |a| a.writable_by?(user) }
  end

  def manager?(user)
    user.in?(managers) || ancestors.any? { |a| a.manageable_by?(user) }
  end

  def set_owner_id
    self.owner_id = parent.present? ? parent.owner_id : creator_id
  end

  def ensure_no_locked_file_is_destroyed
    return unless self_and_descendants.any?(&:has_locked_file?)

    msg = 'Não é possível efetuar esta ação pois existem arquivos trancados ' \
          'nas pastas a serem excluídas'
    errors[:base] << msg
    throw :abort
  end

  def sync_ancestors_updated_at
    ancestors.where('updated_at < ?', updated_at)
             .update_all(updated_at: updated_at)
  end
end
