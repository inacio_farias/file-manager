class AccessPermit < ApplicationRecord
  belongs_to :user
  belongs_to :resource, polymorphic: true

  validates_uniqueness_of :user_id, scope: %i[resource_id resource_type]
  validates :access_level, presence: true
  validates :resource,     presence: true

  before_create :permission_already_given

  ACCESS_LEVELS = %i[manager writer reader].freeze
  enum access_level: ACCESS_LEVELS

  scope :of_user, (->(user) { where(user_id: user.id) })
  scope :order_by_user_name, (-> { includes(:user).order('users.name ASC') })

  private

  def permission_already_given
    return if resource.blank?
    return unless resource.accessible_by?(user)

    errors[:base] = "O usuário #{user.name} já possui acesso a este recurso"
    throw :abort
  end
end
