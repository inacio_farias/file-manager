class RootFolder
  attr_reader :user

  def initialize(user)
    @user = user
  end

  def self.policy_class
    FolderPolicy
  end

  def id; end

  def name
    'Raiz'
  end

  def root?(*)
    true
  end

  def ancestors
    []
  end

  def children
    Folder.where(parent_id: nil, creator: user) | user.shared_folders
  end

  def assets
    Asset.where(folder_id: nil, user: user)
  end

  METHODS_TO_ALIAS = {
    user:  %i[creator owner],
    root?: %i[created_by? accessible_by? readable_by? writable_by?
              manageable_by? owner?]
  }.freeze
  METHODS_TO_ALIAS.each do |ref, methods|
    Array(methods).each { |method| alias_method method, ref }
  end
end
