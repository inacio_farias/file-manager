class Contact < ApplicationRecord
  validates :name, :email, :origin, presence: true

  scope :unprocessed, (-> { where(processed_at: nil) })

  def processed?
    !processed_at.nil?
  end
end
