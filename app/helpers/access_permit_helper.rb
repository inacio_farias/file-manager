module AccessPermitHelper
  def translate_access_level(access_level)
    case String(access_level).downcase
    when 'manager' then 'Gerenciar'
    when 'writer'  then 'Escrita'
    when 'reader'  then 'Leitura'
    else
      raise "Value \"#{access_level}\" not mapped for translation"
    end
  end
end
