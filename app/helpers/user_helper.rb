module UserHelper
  def translate_user_level(user_level)
    case String(user_level).downcase
    when 'read_only' then 'Somente leitura'
    when 'regular'   then 'Usuário regular'
    when 'super'     then 'Administrador'
    else
      raise "Value \"#{user_level}\" not mapped for translation"
    end
  end
end
