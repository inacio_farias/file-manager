module FileTypeHelper
  def get_fyle_type(raw_file_type)
    mapped_types = %i[image video audio pdf sheet doc compressed presentation]
    mapped_types.each do |type|
      return type if send("#{type}?".to_sym, raw_file_type)
    end
  end

  def image?(fyle_type)
    fyle_type.start_with? 'image'
  end

  def video?(fyle_type)
    fyle_type.start_with? 'video'
  end

  def audio?(fyle_type)
    fyle_type.start_with? 'audio'
  end

  def pdf?(fyle_type)
    fyle_type.end_with? 'pdf'
  end

  def sheet?(fyle_type)
    fyle_type.end_with? 'sheet'
  end

  def doc?(fyle_type)
    fyle_type.end_with? 'document'
  end

  def presentation?(fyle_type)
    fyle_type.end_with? 'presentation'
  end

  def compressed?(fyle_type)
    known_compressed_types = %w[zip gzip x-rar tar tar.gz]
    fyle_type.end_with?(*known_compressed_types)
  end
end
