class ControlPanelPolicy < Struct.new(:user, :control_panel)
  attr_reader :user

  def initialize(user, _)
    @user = user
  end

  def show?
    user.admin?
  end

  class Scope < ApplicationPolicy::Scope
    def resolve
      scope
    end
  end
end
