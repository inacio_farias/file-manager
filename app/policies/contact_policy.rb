class ContactPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def create?
    true
  end

  def mark_as_processed?
    process?
  end

  def mark_as_unprocessed?
    process?
  end

  def process?
    admin?
  end

  def destroy?
    admin?
  end
end
