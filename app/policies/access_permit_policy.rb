class AccessPermitPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def create?
    FolderPolicy.new(user, record.resource).manage?
  end

  def update?
    FolderPolicy.new(user, record.resource).manage?
  end

  def destroy?
    FolderPolicy.new(user, record.resource).manage?
  end

  def permitted_attributes_for_create
    %i[resource_id resource_type user_id access_level]
  end

  def permitted_attributes_for_update
    [:access_level]
  end
end
