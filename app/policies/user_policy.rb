class UserPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  attr_reader :current_user, :target_user

  def initialize(current_user, target_user)
    @current_user = current_user
    @target_user  = target_user
  end

  def index?
    admin?
  end

  def new?
    admin?
  end

  def edit?
    admin?
  end

  def create?
    admin?
  end

  def update?
    admin? || current_user == target_user
  end

  def update_password?
    admin? || current_user == target_user
  end

  def destroy?
    admin?
  end

  def permitted_attributes_for_create
    %i[name email admin user_level]
  end

  def permitted_attributes_for_update
    if admin?
      %i[name email admin user_level]
    else
      [:name]
    end
  end

  def permitted_attributes_for_update_password
    if admin? || current_user == target_user
      %i[password password_confirmation]
    else
      []
    end
  end

  private

  def admin?
    super(current_user)
  end
end
