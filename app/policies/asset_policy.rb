class AssetPolicy < ApplicationPolicy
  attr_reader :user, :file

  def initialize(user, file)
    @user = user
    @file = file
  end

  def upload?
    file.persisted? ? update? : create?
  end

  def download?
    true
  end

  def destroy?
    admin?
  end

  def toggle_lock?
    admin?
  end

  private def create?
    true
  end

  private def update?
    admin?
  end

  class Scope < Scope
    def resolve
      scope
    end
  end
end
