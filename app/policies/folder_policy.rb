class FolderPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end

  def show?
    read?
  end

  def create?
    parent = record.fetch(:parent, RootFolder.new(user))
    FolderPolicy.new(user, parent).write?
  end

  def update?
    write?
  end

  def destroy?
    manage?
  end

  def manage?
    !user.read_only? && record.manageable_by?(user)
  end

  def write?
    !user.read_only? && record.writable_by?(user)
  end

  def read?
    record.readable_by?(user)
  end
end
