class HomeController < BaseController
  def index
    redirect_to folders_path
  end
end
