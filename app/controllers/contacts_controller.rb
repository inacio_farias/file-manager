class ContactsController < ControlPanelController
  skip_before_action :verify_authenticity_token, only: :create
  skip_before_action :authenticate_user!, only: :create

  before_action :allow_outer_access, only: :create
  before_action :load_contact, only: %i[mark_as_processed
                                        mark_as_unprocessed
                                        destroy]

  def index
    skip_authorization

    @contacts = Contact.order(
      'CASE WHEN processed_at IS NULL     THEN created_at   END DESC,
       CASE WHEN processed_at IS NOT NULL THEN processed_at END DESC'
    ).decorate
  end

  def create
    skip_authorization

    contact = Contact.new(contact_params)
    status, response = if contact.save
                         [200, {}]
                       else
                         [422, { errors: contact.errors }]
                       end

    render json: response, status: status
  end

  def destroy
    if @contact.destroy
      flash[:notice] = 'Contato excluído com sucesso!'
      redirect_to control_panel_contacts_path
    else
      flash[:error] = @contact.errors.full_messages
      redirect_back(fallback_location: control_panel_contacts_path)
    end
  end

  def mark_as_processed
    authorize @contact

    process!(DateTime.current) do
      flash[:notice] = 'Contato marcado como processado!'
    end
  end

  def mark_as_unprocessed
    authorize @contact

    process!(nil) do
      flash[:notice] = 'Contato marcado como não processado!'
    end
  end

  private

  def process!(processed_value)
    if @contact.update_attributes(processed_at: processed_value)
      yield if block_given?
      redirect_to control_panel_contacts_path
    else
      flash[:error] = @contact.errors.full_messages
      redirect_back(fallback_location: control_panel_contacts_path)
    end
  end

  def load_contact
    @contact ||= Contact.find(params[:id]).decorate
  end

  def contact_params
    params.require(:contact).permit(:name, :email, :origin)
  end

  def allow_outer_access
    headers['Access-Control-Allow-Origin']   = '*'
    headers['Access-Control-Allow-Methods']  = 'POST, PUT, DELETE, GET, OPTIONS'
    headers['Access-Control-Request-Method'] = '*'
    headers['Access-Control-Allow-Headers']  = 'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  end
end
