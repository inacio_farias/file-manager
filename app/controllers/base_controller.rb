class BaseController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_authorized, except: %i[index show]

  layout 'application'

  def resource_not_found!
    unauthorized_or_not_found
  end
end
