class ApplicationController < ActionController::Base
  include Pundit
  protect_from_forgery with: :exception

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  private

  def after_sign_out_path_for(resource)
    new_session_path(resource)
  end

  def user_not_authorized
    unauthorized_or_not_found
  end

  def unauthorized_or_not_found
    msg = "Ooops! A página requisitada não foi encontrada!\n" \
          'Verifique o caminho e tente novamente ou entre '   \
          'em contato com o suporte técnico.'

    flash[:alert] = msg
    redirect_back(fallback_location: root_path)
  end
end
