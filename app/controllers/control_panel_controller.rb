class ControlPanelController < BaseController
  after_action :verify_authorized

  layout 'control_panel'

  def show
    # authorize :control_panel, :show?
    skip_authorization
    redirect_to control_panel_users_path
  end
end
