class CurrentUserController < BaseController
  before_action :validate_current_password, only: :update_password
  before_action :load_user

  def self.controller_path
    'users'
  end

  def profile
    skip_authorization
  end

  def update
    authorize @user

    if @user.update_attributes(permitted_attributes(@user))
      UsersLogService.user_updated(target: @user, user: current_user)

      flash[:notice] = 'Dados atualizados com sucesso!'
      redirect_to profile_current_user_path
    else
      flash[:error] = @user.errors.full_messages
      redirect_back(fallback_location: root_path)
    end
  end

  def update_password
    authorize @user

    if @user.update_attributes(permitted_attributes(@user))
      sign_in(current_user, bypass: true)
      UsersLogService.user_password_updated(target: @user, user: current_user)

      flash[:notice] = 'Senha atualizada com sucesso!'
      redirect_to profile_current_user_path
    else
      flash[:error] = @user.errors.full_messages
      redirect_back(fallback_location: root_path)
    end
  end

  protected

  def load_user
    @user ||= current_user
  end

  private

  def validate_current_password
    current_psw = params[:user].delete(:current_password)
    return if @user.valid_password?(current_psw)

    flash[:error] = 'A senha atual está incorreta'
    redirect_back(fallback_location: root_path)
  end
end
