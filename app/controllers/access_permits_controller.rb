class AccessPermitsController < BaseController
  before_action :load_folder
  before_action :load_permission, except: :create
  before_action :init_permission, only: :create

  def create
    @permission.assign_attributes(permitted_attributes(@permission))
    authorize @permission

    if @permission.save
      AccessPermitsLogService.access_permit_created(permit: @permission,
                                                    user: current_user)
      flash[:notice] = 'Permissão criada com sucesso'
    else
      flash[:error] = @permission.errors.full_messages
    end
    redirect_back(fallback_location: root_path)
  end

  def update
    authorize @permission

    if @permission.update_attributes(permitted_attributes(@permission))
      AccessPermitsLogService.access_permit_updated(permit: @permission,
                                                    user: current_user)
      flash[:notice] = 'Permissão atualizada com sucesso'
    else
      flash[:error] = @permission.errors.full_messages
    end
    redirect_back(fallback_location: root_path)
  end

  def destroy
    authorize @permission

    if @permission.destroy
      AccessPermitsLogService.access_permit_destroyed(permit: @permission,
                                                      user: current_user)
      flash[:notice] = 'Permissão excluída com sucesso'
    else
      flash[:error] = @permission.errors.full_messages
    end
    redirect_back(fallback_location: root_path)
  end

  private

  def load_folder
    @folder ||= Folder.where(id: params[:folder_id]).first.decorate
    resource_not_found! if @folder.blank?
  end

  def init_permission
    @permission ||= AccessPermit.new
  end

  def load_permission
    @permission ||= AccessPermit.where(id: params[:id]).first
    resource_not_found! if @permission.blank?
  end
end
