class FoldersController < BaseController
  before_action :load_folder, only: %i[destroy update]
  before_action :load_decorated_folder, only: %i[show manage]

  def index
    @folder = FolderDecorator.decorate(RootFolder.new(current_user))
    render :show
  end

  def manage
    authorize @folder
  end

  def show
    authorize @folder
  end

  def create
    @folder = Folder.new(folder_params.merge(creator: current_user))
    authorize @folder

    if @folder.save
      FoldersLogService.folder_created(folder: @folder, user: current_user)

      flash[:notice] = 'Pasta criada com sucesso!'
      redirect_to redirect_path
    else
      flash[:error] = @folder.errors.full_messages
      redirect_back(fallback_location: root_path)
    end
  end

  def update
    authorize @folder

    if @folder.update(folder_params)
      FoldersLogService.folder_updated(folder: @folder, user: current_user)
      flash[:notice] = 'Nome atualizado com sucesso!'
    else
      flash[:error] = @folder.errors.full_messages
    end

    redirect_back(fallback_location: redirect_path)
  end

  def destroy
    authorize @folder

    if @folder.destroy
      FoldersLogService.folder_destroyed(folder: @folder, user: current_user)

      flash[:notice] = 'Pasta excluída com sucesso!'
      redirect_to redirect_path
    else
      flash[:error] = @folder.errors.full_messages
      redirect_back(fallback_location: redirect_path)
    end
  end

  private

  def redirect_path
    @folder.parent.present? ? @folder.parent : root_path
  end

  def folder_params
    params.require(:folder).permit(:parent_id, :name)
  end

  def load_decorated_folder
    load_folder unless @folder
    @folder = @folder.decorate
  end

  def load_folder
    @folder = Folder.where(id: params[:id]).first
    resource_not_found! if @folder.blank?
  end
end
