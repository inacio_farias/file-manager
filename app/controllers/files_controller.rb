class FilesController < BaseController
  before_action :load_asset
  before_action :redirect_to_404, except: :upload

  def upload
    @asset ||= Asset.new(user: current_user)
    authorize @asset

    if @asset.update(file_params)
      if @asset.previous_changes.keys.include?('id')
        AssetsLogService.asset_created(asset: @asset, user: current_user)
      else
        AssetsLogService.asset_updated(asset: @asset, user: current_user)
      end

      flash[:notice] = 'Arquivo salvo com sucesso!'
      redirect_to redirect_path
    else
      flash[:error] = @asset.errors.full_messages
      redirect_back(fallback_location: root_path)
    end
  end

  def download
    if @asset
      authorize @asset
      AssetsLogService.asset_downloaded(asset: @asset, user: current_user)

      send_file @asset.uploaded_file.path, type: @asset.file_type
    else
      flash[:error] = 'Arquivo não encontrado'
      redirect_back(fallback_location: root_path)
    end
  end

  def destroy
    authorize @asset

    if @asset.destroy
      AssetsLogService.asset_destroyed(asset: @asset, user: current_user)

      flash[:notice] = 'Arquivo excluído com sucesso!'
      redirect_to redirect_path
    else
      flash[:error] = @asset.errors.full_messages
      redirect_back(fallback_location: root_path)
    end
  end

  def toggle_lock
    authorize @asset

    if @asset.locked?
      unlock!
    else
      lock!
    end
  end

  private

  def lock!
    lock = AssetLock.new(user: current_user, asset: @asset)

    if lock.save
      AssetsLogService.asset_locked(asset: @asset, user: current_user)

      flash[:notice] = 'Arquivo trancado com sucesso!'
      redirect_to redirect_path
    else
      flash[:error] = @asset.errors.full_messages
      redirect_back(fallback_location: root_path)
    end
  end

  def unlock!
    lock = @asset.asset_lock

    if lock.destroy
      AssetsLogService.asset_unlocked(asset: @asset, user: current_user)

      flash[:notice] = 'Arquivo destrancado com sucesso!'
      redirect_to redirect_path
    else
      flash[:error] = @asset.errors.full_messages
      redirect_back(fallback_location: root_path)
    end
  end

  def redirect_path
    folder = @asset.folder
    folder ? folder_path(folder) : folders_path
  end

  def file_params
    params.require(:asset).permit(:uploaded_file, :folder_id, :original_name)
  end

  def load_asset
    @asset = Asset.where(id: params[:id]).first
  end

  def redirect_to_404
    resource_not_found! if @asset.nil?
  end
end
