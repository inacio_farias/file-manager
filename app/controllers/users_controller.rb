class UsersController < ControlPanelController
  before_action :init_user, only: %i[new create]
  before_action :load_user, except: %i[index new create]
  before_action :authorize_user, except: [:index]

  def index
    authorize current_user
    @users = User.all.order(:name).decorate
  end

  def new; end

  def edit; end

  def create
    attrs = permitted_attributes(@user).merge(psw_default_params)

    if @user.update_attributes(attrs)
      UsersLogService.user_created(target: @user, user: current_user)

      msg = 'Usuário criado com sucesso! A senha inicial é "123456". ' \
            'Por favor altere-a agora ou instrua o novo usuário a '    \
            'alterá-la no primeiro acesso.'

      flash[:notice] = msg
      redirect_to control_panel_users_path(@user)
    else
      flash[:error] = @user.errors.full_messages
      redirect_back(fallback_location: control_panel_users_path)
    end
  end

  def update
    if @user.update_attributes(permitted_attributes(@user))
      UsersLogService.user_updated(target: @user, user: current_user)

      flash[:notice] = 'Usuário atualizado com sucesso!'
      redirect_to control_panel_users_path
    else
      flash[:error] = @user.errors.full_messages
      redirect_back(fallback_location: control_panel_users_path)
    end
  end

  def update_password
    if @user.update_attributes(permitted_attributes(@user))
      sign_in(current_user, bypass: true)
      UsersLogService.user_password_updated(target: @user, user: current_user)

      flash[:notice] = 'Senha atualizada com sucesso!'
      redirect_to control_panel_users_path
    else
      flash[:error] = @user.errors.full_messages
      redirect_back(fallback_location: control_panel_users_path)
    end
  end

  def destroy
    if @user.destroy
      UsersLogService.user_destroyed(target: @user, user: current_user)

      flash[:notice] = 'Usuário apagado com sucesso!'
      redirect_to control_panel_users_path
    else
      flash[:error] = @user.errors.full_messages
      redirect_back(fallback_location: control_panel_users_path)
    end
  end

  protected

  def init_user
    @user ||= User.new
  end

  def load_user
    @user ||= User.where(id: params[:id]).first
  end

  def authorize_user
    authorize @user
  end

  private

  def psw_default_params
    psw = '123456'
    { password: psw, password_confirmation: psw }
  end
end
