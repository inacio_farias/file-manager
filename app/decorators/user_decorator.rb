class UserDecorator < Draper::Decorator
  delegate_all

  def admin_to_pt
    @object.admin? ? 'SIM' : 'NÃO'
  end
end
