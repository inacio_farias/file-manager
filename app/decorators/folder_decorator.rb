class FolderDecorator < Draper::Decorator
  delegate_all

  def display_content
    if empty?
      empty_folder_content
    else
      not_empty_folder_content
    end
  end

  def icon
    h.content_tag :span, class: 'margin-r-md' do
      h.content_tag :i, class: 'fa fa-lg fa-folder-o' do
      end
    end
  end

  def author
    object.creator.name
  end

  def size; end

  def creation_time
    object.created_at.strftime('%d/%m/%Y às %H:%M')
  end

  def last_modified
    object.updated_at.strftime('%d/%m/%Y às %H:%M')
  end

  def children
    object.children.sort_by(&:name)
  end

  def new_folder_path
    object.root? ? h.folders_path : h.new_inner_folder_path(object.id)
  end

  def modal_id
    @unique_identifier ||= SecureRandom.hex(16)
  end

  def object_class
    object.class
  end

  private

  def empty?
    !(children.any? || assets.exists?) # exists? is fast as hell! Use it!!
  end

  def empty_folder_content
    h.content_tag :h2, class: 'align-center margin-v-lg' do
      if h.current_user.read_only? && object.root?
        'Nenhum arquivo compartilhado com você'
      else
        'Esta pasta está vazia'
      end
    end
  end

  def not_empty_folder_content
    locals = { folders: children.map(&:decorate), files: assets.decorate }
    h.render partial: 'folder_content', locals: locals
  end

  def assets
    object.assets.order(:uploaded_file_file_name)
  end
end
