class AccessPermitDecorator < Draper::Decorator
  delegate_all

  def access_level_translated
    h.translate_access_level access_level
  end

  def modal_id
    @unique_identifier ||= SecureRandom.hex(16)
  end
end
