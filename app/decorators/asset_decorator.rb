class AssetDecorator < Draper::Decorator
  delegate_all

  def icon
    h.content_tag :span, class: 'margin-r-md' do
      h.content_tag :i, class: "fa fa-lg #{file_icon(object.file_type)}" do
      end
    end
  end

  def name
    object.uploaded_file_file_name
  end

  def author
    object.user.name
  end

  def size
    h.number_to_human_size(object.file_size, precision: 2)
  end

  def creation_time
    object.created_at.strftime('%d/%m/%Y às %H:%M')
  end

  def last_modified
    object.updated_at.strftime('%d/%m/%Y às %H:%M')
  end

  private

  def file_icon(type)
    case h.get_fyle_type(type)
    when :image        then 'fa-file-image-o'
    when :video        then 'fa-file-video-o'
    when :audio        then 'fa-file-audio-o'
    when :pdf          then 'fa-file-pdf-o'
    when :sheet        then 'fa-file-excel-o'
    when :doc          then 'fa-file-word-o'
    when :compressed   then 'fa-file-archive-o'
    when :presentation then 'fa-file-powerpoint-o'
    else 'fa-file-o'
    end
  end
end
