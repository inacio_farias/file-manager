class ContactDecorator < Draper::Decorator
  delegate_all

  def origin_text
    mapped_origins = {
      1 => 'Site'
    }

    mapped_origins[object.origin]
  end

  def formatted_created_at
    formatted(:created_at)
  end

  def formatted_processed_at
    processed_at.present? ? formatted(:processed_at) : '---'
  end

  def contact_processing_link
    h.link_to contact_processing_path, method: :post do
      if processed?
        h.content_tag :i, class: 'fa fa-check-square-o fa-fw fa-lg',
                          title: 'Marcar como não processado' do; end
      else
        h.content_tag :i, class: 'fa fa-square-o fa-fw fa-lg',
                          title: 'Marcar como processado' do; end
      end
    end
  end

  def contact_processing_path
    if processed?
      h.mark_as_unprocessed_control_panel_contact_path(object)
    else
      h.mark_as_processed_control_panel_contact_path(object)
    end
  end

  private

  def formatted(date)
    send(date).strftime('%d/%m/%Y às %H:%M')
  end
end
