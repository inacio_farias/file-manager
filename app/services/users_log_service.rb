class UsersLogService
  class << self
    def user_created(target:, user:)
      Log::UserCreated.create(
        target: target,
        user:   user,
        recorded_changes: target.previous_changes
      )
    end

    def user_updated(target:, user:)
      Log::UserUpdated.create(
        target: target,
        user:   user,
        recorded_changes: target.previous_changes
      )
    end

    def user_password_updated(target:, user:)
      Log::UserPasswordUpdated.create(
        target: target,
        user:   user,
        recorded_changes: {}
      )
    end

    def user_destroyed(target:, user:)
      Log::UserDestroyed.create(
        target: target,
        user:   user,
        recorded_changes: {}
      )
    end
  end
end
