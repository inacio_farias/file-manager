class AssetsLogService
  class << self
    def asset_created(asset:, user:)
      Log::AssetCreated.create(
        asset: asset,
        user:  user,
        recorded_changes: asset.previous_changes
      )
    end

    def asset_updated(asset:, user:)
      Log::AssetUpdated.create(
        asset: asset,
        user:  user,
        recorded_changes: asset.previous_changes
      )
    end

    def asset_destroyed(asset:, user:)
      Log::AssetDestroyed.create(
        asset: asset,
        user:  user,
        recorded_changes: {}
      )
    end

    def asset_downloaded(asset:, user:)
      Log::AssetDownloaded.create(
        asset: asset,
        user:  user,
        recorded_changes: {}
      )
    end

    def asset_locked(asset:, user:)
      Log::AssetLocked.create(
        asset: asset,
        user:  user,
        recorded_changes: {}
      )
    end

    def asset_unlocked(asset:, user:)
      Log::AssetUnlocked.create(
        asset: asset,
        user:  user,
        recorded_changes: {}
      )
    end
  end
end
