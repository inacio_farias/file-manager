class FoldersLogService
  class << self
    def folder_created(folder:, user:)
      Log::FolderCreated.create(
        folder: folder,
        user:   user,
        recorded_changes: folder.previous_changes
      )
    end

    def folder_updated(folder:, user:)
      Log::FolderUpdated.create(
        folder: folder,
        user:   user,
        recorded_changes: folder.previous_changes
      )
    end

    def folder_destroyed(folder:, user:)
      Log::FolderDestroyed.create(
        folder: folder,
        user:   user,
        recorded_changes: {}
      )
    end
  end
end
