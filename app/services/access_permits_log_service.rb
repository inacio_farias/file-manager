class AccessPermitsLogService
  class << self
    def access_permit_created(permit:, user:)
      Log::AccessPermitCreated.create(
        access_permit: permit,
        user:   user,
        recorded_changes: permit.previous_changes
      )
    end

    def access_permit_updated(permit:, user:)
      Log::AccessPermitUpdated.create(
        access_permit: permit,
        user:   user,
        recorded_changes: permit.previous_changes
      )
    end

    def access_permit_destroyed(permit:, user:)
      Log::AccessPermitDestroyed.create(
        access_permit: permit,
        user:   user,
        recorded_changes: {}
      )
    end
  end
end
