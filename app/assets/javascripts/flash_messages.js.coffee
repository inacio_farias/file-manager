$ ->
  bar = $('#flash-holder')
  closeFlashMessage = ()->
    bar.animate(
      top: "-#{bar.outerHeight()}px",
      opacity: '0'
      ()-> @remove()
    )

  $(document).on 'click', '#flash-holder', closeFlashMessage
  setTimeout(
    ()-> closeFlashMessage()
    2500
  ) unless bar.children('.error').length
